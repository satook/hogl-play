#version 330

smooth in vec2 texUV;

out vec4 color;

uniform sampler2D diffuseTex;

void main(){
    color = texture(diffuseTex, texUV);
}
