#version 330

uniform vec2 camerapos;
uniform vec2 halfscreensize;

in vec2 position;
in vec2 uv;

smooth out vec2 texUV;

void main() {
    gl_Position.xy = (position - camerapos)/halfscreensize;
    gl_Position.z = 0.5;
    gl_Position.w = 1.0;
    texUV = uv;
}
