module Main where

-- everything from here starts with gl or GL
import qualified Graphics.UI.GLFW as GLFW
import Graphics.GL.Core33
import GLUtil
import Draw
import Cells
import Control.Monad ( unless )
import Control.Exception ( finally )

initGL :: GLFW.Window -> IO ()
initGL win = do
  version <- getString GL_VERSION
  putStrLn ("GL context created. Version: " ++ version)
  glClearColor 1.0 0 0 0 -- Clear the background color to black
  glDisable GL_CULL_FACE
  glDisable GL_DEPTH_TEST
  checkGLErrors "Setting common settings"
  (w,h) <- GLFW.getFramebufferSize win
  resizeScene win w h

resizeScene :: GLFW.WindowSizeCallback
resizeScene win w     0      = resizeScene win w 1 -- prevent divide by zero
resizeScene _   width height = do
  glViewport 0 0 (fromIntegral width) (fromIntegral height)

windowRefresh :: DrawCtx -> [SpriteAndPos] -> GLuint -> GLFW.Window -> IO ()
windowRefresh ctx sprs atlasTex _ = drawScene ctx sprs atlasTex

drawScene :: DrawCtx -> [SpriteAndPos] -> GLuint -> IO ()
drawScene ctx sprs atlasTex = do
  drawStart ctx (320.0, 240.0)
  -- draw level
  drawSprites ctx sprs atlasTex
  -- draw entities

  drawEnd ctx

keyPressed :: GLFW.KeyCallback
keyPressed win GLFW.Key'Escape _ GLFW.KeyState'Pressed _ = GLFW.setWindowShouldClose win True
keyPressed _   _               _ _                     _ = return ()

-- process a single Game loop
runGame :: GLFW.Window -> DrawCtx -> [SpriteAndPos] -> GLuint -> IO ()
runGame win ctx sprs atlasTex = do
  -- get input
  -- turn in to actions
  GLFW.pollEvents

  -- run sim
    -- apply input actions
    -- sim movement/collision
    -- AI
    -- level logic

  -- draw scene
  drawScene ctx sprs atlasTex

  -- swap the buffers over
  GLFW.swapBuffers win

  shouldClose <- GLFW.windowShouldClose win
  unless shouldClose $ do
    runGame win ctx sprs atlasTex

printError :: GLFW.Error -> String -> IO ()
printError _ s = do
  putStrLn s

main :: IO ()
main = do
  True <- GLFW.init
  GLFW.setErrorCallback $ Just $ printError
  GLFW.defaultWindowHints
  GLFW.windowHint (GLFW.WindowHint'ContextVersionMajor 3)  -- we want OpenGL 3.3
  GLFW.windowHint (GLFW.WindowHint'ContextVersionMinor 3)
  GLFW.windowHint (GLFW.WindowHint'ClientAPI GLFW.ClientAPI'OpenGL)
  GLFW.windowHint (GLFW.WindowHint'OpenGLForwardCompat True)
  GLFW.windowHint (GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core)

  -- get a 640 x 480 window
  win <- GLFW.createWindow 640 480 "OglPlay" Nothing Nothing
  runWith win

  where
    runWith Nothing = putStrLn "ERROR: Could not create OpenGL Context. Quitting."
    runWith (Just win) = do
      GLFW.makeContextCurrent (Just win)
      checkGLErrors "Binding GL context"

      -- init our global GL state
      initGL win
      checkGLErrors "Setting initial GL state"
      ctx <- buildDrawCtx

      -- make a test sprite
      Right atlasTex <- pathToTex "Data/atlas.png"
      let sprs = fmap (\i -> (SpriteAndPos (Sprite $ rem i 9) ((fromIntegral $ rem i 20)*32.0, (fromIntegral $ quot i 20)*32.0))) [0..341]

      -- callback functions
      GLFW.setWindowRefreshCallback win $ Just $ windowRefresh ctx sprs atlasTex
      GLFW.setFramebufferSizeCallback win $ Just $ resizeScene
      GLFW.setKeyCallback win $ Just $ keyPressed

      -- make sure we clean up
      finally (runGame win ctx sprs atlasTex) $ do
        putStrLn ("Cleaning up context and GLFW window.")
        -- clean up
        GLFW.destroyWindow win
        GLFW.terminate
