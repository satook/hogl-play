module GLUtil ( getString
              , withGLString
              , loadShader
              , checkShaderProgram
              , checkGLErrors
              , getUniformLocation
              , genGLObjects
              , withMappedBufferRange
              , marshalGLboolean
              , mapBufferRange
              , unmapBuffer
              , MapBufferUsage(..)
              , MappingFailure(..)
              ) where

import Graphics.GL.Core33

import Control.Monad
import Foreign
import Foreign.C.String
import Debug.Trace ( traceStack )

import Data.IORef ( newIORef, readIORef, writeIORef )
import Control.Exception ( finally )

checkGLErrors :: String -> IO ()
checkGLErrors s = do
    err <- glGetError

    unless (err == GL_NO_ERROR) $ do
      traceStack ("Error in " ++ s ++ ": " ++ (show err)) $ return ()

    return ()

getString :: GLenum -> IO String
getString n = do
    ptr <- glGetString n
    if ptr == nullPtr
        then return ""
        else peekCString (castPtr ptr)

withGLString :: String -> (Ptr GLchar -> IO a) -> IO a
withGLString s action = withCString s $ action

-- |Use this to call glGen* functions and return a
genGLObjects :: (GLsizei -> Ptr GLuint -> IO ()) -> Int -> IO [GLuint]
genGLObjects f count = do
  -- allocate an array for the objects
  allocaArray count $ \ptr -> do
    let glcount = fromIntegral count
    f glcount ptr
    peekArray count ptr

getUniformLocation :: GLuint -> String -> IO GLint
getUniformLocation prog str = withGLString str $ glGetUniformLocation prog

checkShaderProgram :: GLuint -> IO ()
checkShaderProgram prog = do
  infoLogLength <- alloca $ \buf -> do
    glGetProgramiv prog GL_INFO_LOG_LENGTH buf
    peek buf
  infoLog <- alloca $ \len ->
    allocaBytes (fromIntegral infoLogLength) $ \chars -> do
      glGetProgramInfoLog prog infoLogLength len chars
      len' <- fromIntegral <$> peek len
      peekCStringLen (chars, len')
  unless (null infoLog) (print infoLog)
  return ()

loadShader :: FilePath -> GLenum-> IO GLuint
loadShader filePath kind = do
  shaderSrc <- readFile filePath
  shader <- glCreateShader kind
  withCStringLen shaderSrc $ \(src,len) ->
    withArray [src] $ \buf ->
      withArray [fromIntegral len] $ \l ->
        glShaderSource shader 1 buf l
  glCompileShader shader

  ok <- alloca $ \buf -> do
          glGetShaderiv shader GL_COMPILE_STATUS buf
          fmap (> 0) (peek buf)
  infoLogLen <- alloca $ \ptr -> do glGetShaderiv shader GL_INFO_LOG_LENGTH ptr
                                    peek ptr
  infoLog <- alloca $ \len ->
               allocaBytes (fromIntegral infoLogLen) $ \chars -> do
                 glGetShaderInfoLog shader infoLogLen len chars
                 len' <- fromIntegral <$> peek len
                 peekCStringLen (chars, len')
  unless (null infoLog)
         (mapM_ putStrLn
                ["Shader info log for '" ++ filePath ++ "':", infoLog, ""])
  unless ok $ glDeleteShader shader
  return shader

maybeNullPtr :: b -> (Ptr a -> b) -> Ptr a -> b
maybeNullPtr n f ptr | ptr == nullPtr = n
                     | otherwise      = f ptr

marshalGLboolean :: Num a => Bool -> a
marshalGLboolean x = fromIntegral $ case x of
   False -> GL_FALSE
   True -> GL_TRUE

unmarshalGLboolean :: (Eq a, Num a) => a -> Bool
unmarshalGLboolean = (/= fromIntegral GL_FALSE)

data MapBufferUsage =
     Read
   | Write
   | InvalidateRange
   | InvalidateBuffer
   | FlushExplicit
   | Unsychronized
   deriving ( Eq, Ord, Show )

marshalMapBufferUsage :: MapBufferUsage -> GLbitfield
marshalMapBufferUsage x = case x of
    Read -> GL_MAP_READ_BIT
    Write -> GL_MAP_WRITE_BIT
    InvalidateRange -> GL_MAP_INVALIDATE_RANGE_BIT
    InvalidateBuffer -> GL_MAP_INVALIDATE_BUFFER_BIT
    FlushExplicit -> GL_MAP_FLUSH_EXPLICIT_BIT
    Unsychronized -> GL_MAP_UNSYNCHRONIZED_BIT

data MappingFailure =
     MappingFailed
   | UnmappingFailed
   deriving ( Eq, Ord, Show )

-- | Convenience function for an exception-safe combination of 'mapBufferRange' and
-- 'unmapBuffer'.
withMappedBufferRange ::
  GLenum -> GLintptr -> GLsizeiptr -> [MapBufferUsage] -> (Ptr a -> IO b) -> (MappingFailure -> IO b) -> IO b
withMappedBufferRange t o l u action err = do
  maybeBuf <- mapBufferRange t o l u
  case maybeBuf  of
    Nothing -> err MappingFailed
    Just buf -> do (ret, ok) <- action buf `finallyRet` unmapBuffer t
                   if ok
                      then return ret
                      else err UnmappingFailed

mapBufferRange_ ::
   GLenum -> GLintptr -> GLsizeiptr -> [MapBufferUsage] -> IO (Ptr a)
mapBufferRange_ t o l a = glMapBufferRange t o l
   (sum (map marshalMapBufferUsage a))

mapBufferRange ::
   GLenum -> GLintptr -> GLsizeiptr -> [MapBufferUsage] -> IO (Maybe (Ptr a))
mapBufferRange t o l a =
   fmap (maybeNullPtr Nothing Just) $ mapBufferRange_ t o l a

unmapBuffer :: GLenum -> IO Bool
unmapBuffer = fmap unmarshalGLboolean . glUnmapBuffer

{-# INLINE finallyRet #-}
finallyRet :: IO a -> IO b -> IO (a, b)
a `finallyRet` sequel = do
   r2Ref <- newIORef undefined
   r1 <- a `finally` (sequel >>= writeIORef r2Ref)
   r2 <- readIORef r2Ref
   return (r1, r2)
