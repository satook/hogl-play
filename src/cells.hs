module Cells ( imgToTex
             , pathToTex
             ) where

import GLUtil
import Codec.Picture ( readImage )
import Codec.Picture.Types
import Foreign.Ptr ( Ptr )
import Graphics.GL.Core33
import Data.Vector.Storable ( unsafeWith )

{-
data CellIndex = CellIndex GLuint

data CellSet = CellSet {
                       }

-- |Load a new cell at path and add it to the CellSet, returning the newly
-- updated CellSet and a CellIndex to refer to the cell.
addCellToSet :: CellSet -> String -> IO (CellSet, Either String CellIndex)
addCellToSet cs s = do
    return (cs, Left ("Not implemented" ++ s))

    -- if it's already there, grab that index, return it
    -- else load it, add it, return new set
-}
-- load image
-- push into GL texture
-- return text name

pathToTex :: String -> IO (Either String GLuint)
pathToTex s = readImage s >>= either (return . Left) aux
    where aux img = Right <$> imgToTex img

imgToTex :: DynamicImage -> IO GLuint
imgToTex img = do
    [texId] <- genGLObjects glGenTextures 1
    glBindTexture GL_TEXTURE_2D texId
    loadAux img
    return texId

    where
        loadAux (ImageY8     (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_R8     GL_RED  GL_UNSIGNED_BYTE  ptr w h
        loadAux (ImageY16    (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_R16    GL_RED  GL_UNSIGNED_SHORT ptr w h
        loadAux (ImageYF     (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_R32F   GL_RED  GL_FLOAT          ptr w h
        loadAux (ImageYA8    (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RG8    GL_RG   GL_UNSIGNED_BYTE  ptr w h
        loadAux (ImageYA16   (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RG16   GL_RG   GL_UNSIGNED_SHORT ptr w h
        loadAux (ImageRGB8   (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RGB8   GL_RGB  GL_UNSIGNED_BYTE  ptr w h
        loadAux (ImageRGB16  (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RGB16  GL_RGB  GL_UNSIGNED_SHORT ptr w h
        loadAux (ImageRGBF   (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RGB32F GL_RGB  GL_FLOAT          ptr w h
        loadAux (ImageRGBA8  (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RGBA8  GL_RGBA GL_UNSIGNED_BYTE  ptr w h
        loadAux (ImageRGBA16 (Image w h p)) = unsafeWith p $ \ptr -> loadTexData GL_RGBA16 GL_RGBA GL_UNSIGNED_SHORT ptr w h
        loadAux (ImageYCbCr8 imd)           = loadAux . ImageRGB8  $ convertImage imd
        loadAux (ImageCMYK8  imd)           = loadAux . ImageRGB8  $ convertImage imd
        loadAux (ImageCMYK16 imd)           = loadAux . ImageRGB16 $ convertImage imd

loadTexData :: GLenum -> GLenum -> GLenum -> Ptr a -> Int -> Int -> IO ()
loadTexData ifo efo ety ptr width height = do
    let ifoGL  = fromIntegral ifo
    let [w, h] = map fromIntegral [width, height]
    glTexImage2D GL_TEXTURE_2D 0 ifoGL w h 0 efo ety ptr
    checkGLErrors "Loading texture from image"
    glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S (fromIntegral GL_REPEAT)
    glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T (fromIntegral GL_REPEAT)
    glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER (fromIntegral GL_LINEAR_MIPMAP_NEAREST)
    glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER (fromIntegral GL_LINEAR)
    checkGLErrors "Setting Tex params for image"
    glGenerateMipmap GL_TEXTURE_2D
    checkGLErrors "Generating MipMaps for image"
    return ()
