module IslandGen ( Point
                 , LineInf
                 , genLineInf
                 ) where

import System.Random

-- process
  -- generate a jagged line
  -- put some dots randomly on it
  -- generate different islands along it

-- line gen?
  -- random number of segments
  -- start in the centre
  -- pick a random angle
  -- add segments to each end alternating
  --

-- island locations?
  --

-- islands?
  --

-- Point (x, y)
type Point = (Double, Double)
-- Line segment (Length, Relative angle)
type SegInf = (Double, Double)
-- Line information (Starting point, length, angle CW from Up)
type LineInf = (Point, Double, Double)

-- gen segCount (even number between 2-8)
-- gen segCount line lengths
-- gen starting angle
-- gen segCount-1 angles (random between -pi/4 and pi/4 degrees)
-- start at (0,0) 0 ([angles], [lengths])
--
-- start at line end,
-- turn into a list of points

genFaultLine :: (RandomGen g) => g -> Int -> ([LineInf], g)
genFaultLine gen segCount = (genLineInf pairs, gen''')
    where
        pairs = zip lengths' angles'
        (lengths, gen') = finiteRandomRs segCount (1, 4) gen
        lengths' = map (\x -> x*2) lengths
        (angles, gen'') = finiteRandomRs (segCount-1) (-45, 45) gen'
        (startAngle, gen''') = randomR (0, 360) gen''
        angles' = startAngle:angles

faultLength :: [LineInf] -> Double
faultLength = foldr proc 0
  where
    proc (_, l, _) a = a + l

finiteRandomRs :: (RandomGen g, Random a, Num n, Eq n) => n -> (a, a) -> g -> ([a], g)
finiteRandomRs 0 _ gen = ([], gen)
finiteRandomRs n range gen =
  let (value, newgen) = randomR range gen
      (rest, lastgen) = finiteRandomRs (n-1) range newgen
  in  (value:rest, lastgen)

-- make a list of relative segments into (start, length, angle) items
genLineInf :: [SegInf] -> [LineInf]
genLineInf = genLineInf' (0, 0) 0

genLineInf' :: Point -> Double -> [SegInf] -> [LineInf]
genLineInf' _ _ [] = []
genLineInf' start sa (x:xs) =
  let rest = genLineInf' end absAng xs
  in  (start, len, absAng):rest
  where
    (len, relAng) = x
    (sx, sy) = start
    absAng = sa + relAng
    end = (sx + len*(cosdeg absAng), sy + len*(sindeg absAng))

cosdeg t = cos (t*pi/180)
sindeg t = sin (t*pi/180)
