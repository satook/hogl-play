
-- Generate an island.

-- oceans?
-- rivers
-- mountains
-- cliff runs
-- grassy plains?
-- caves?
-- bays?
-- flora
-- fauna
-- zones?
    -- tropical
    -- tundra
    -- temperate
    -- temperatue
    -- moisture
    --

data CellGenInfo = CellGenInfo {
    height :: Float
} deriving (Show)

grassSpr :: GLuint
grassSpr = 0
tallGrassSpr :: GLuint
tallGrassSpr = 13 + 128
reedsSpr :: GLuint
reedsSpr = 19 + 256
sandSpr :: GLuint
sandSpr = 21
dirtSpr :: GLuint
dirtSpr = 5
waterSpr :: GLuint
waterSpr = 7
stoneSpr :: GLuint
stoneSpr = 3

-- simple rand sample
-- >= 0.3 => grass
-- < 0.3 => water

mapWidth :: GLuint
mapWidth = 64

data RandomSeed int

generateTerrain :: Seed -> [SpriteAndPos]
generateTerrain =

genRow :: int -> [SpriteAndPos]
genRow y = fmap rowTile [0...mapWidth]
    where rowTile = genTile y

genTile :: int -> int -> SpriteAndPos
genTile y x =

addPos spr y x = SpriteAndPos (Sprite spr) ((fromIntegral $ x)*32.0, (fromIntegral $ y)*32.0)
