module Draw ( Sprite (..)
            , SpriteAndPos (..)
            , DrawCtx
            , buildDrawCtx
            , drawStart
            , drawSprites
            , drawEnd
            ) where

import Graphics.GL.Core33
import Foreign
import Foreign.C.Types

-- my helpers
import GLUtil

vBuffSize :: GLuint
vBuffSize = 1024*1024*8 -- 8Mib vbuffer

cellX :: GLfloat
cellX = 32.0
cellY :: GLfloat
cellY = 32.0
cellSizeTx :: GLuint
cellSizeTx = 32
cellBorderTx :: GLuint
cellBorderTx = 0
atlasWidthTx :: GLuint
atlasWidthTx = 2048
atlasHeightTx :: GLuint
atlasHeightTx = 2048
atlasWidthCells :: GLuint
atlasWidthCells = quot atlasWidthTx (cellSizeTx + cellBorderTx)
atlasLeftMul :: GLfloat
atlasLeftMul = (fromIntegral (cellSizeTx + cellBorderTx)) / (fromIntegral atlasWidthTx)
atlasRightInc :: GLfloat
atlasRightInc = (fromIntegral cellSizeTx) / (fromIntegral atlasWidthTx)
atlasBottomMul :: GLfloat
atlasBottomMul = (fromIntegral (cellSizeTx + cellBorderTx)) / (fromIntegral atlasHeightTx)
atlasTopInc :: GLfloat
atlasTopInc = (fromIntegral cellSizeTx) / (fromIntegral atlasHeightTx)

-- Sprite can be drawn
data Sprite = Sprite { spriteTex :: GLuint
                     } deriving (Show)

data SpriteAndPos = SpriteAndPos { sprite :: Sprite
                                 , pos :: (GLfloat, GLfloat)
                                 } deriving (Show)

buildDrawProgram :: GLuint -> GLuint -> IO (Maybe GLuint)
buildDrawProgram posIndex uvIndex = do
    vertexShader <- loadShader "Data/draw.vert" GL_VERTEX_SHADER
    checkGLErrors "Loading the vert shader"
    fragmentShader <- loadShader "Data/draw.frag" GL_FRAGMENT_SHADER
    checkGLErrors "Loading the frag shader"

    shaderProgram <- glCreateProgram
    checkGLErrors "Creating the shader program"
    glAttachShader shaderProgram vertexShader
    checkGLErrors "Attaching the vert shader"
    glAttachShader shaderProgram fragmentShader
    checkGLErrors "Attaching the frag shader"
    withGLString "position" $ glBindAttribLocation shaderProgram posIndex
    withGLString "uv" $ glBindAttribLocation shaderProgram uvIndex
    glLinkProgram shaderProgram
    checkGLErrors "Linking the shader program"

    checkShaderProgram shaderProgram

    return (Just shaderProgram)

data DrawCtx = DrawCtx { shaderProg :: GLuint
                       , vertSpecArray :: GLuint
                       , vertBuffObj :: GLuint
                       } deriving (Show)

-- |Build a new draw context for drawing sprites/squares. The OpenGL context
-- that will be used for all subsequent draw calls must be already be bound.
buildDrawCtx :: IO DrawCtx
buildDrawCtx = do
    -- vertex binding indexes
    let [posIndex, uvIndex] = [0, 1]

    -- create the shader program
    Just prog <- buildDrawProgram posIndex uvIndex
    checkGLErrors "Building shader program"

    -- create the vao
    [vao] <- genGLObjects glGenVertexArrays 1
    checkGLErrors "Generating Vertex Array"

    -- create the vbo
    [vbuff] <- genGLObjects glGenBuffers 1
    checkGLErrors "Generating Vertex Buffer"

    -- pre-allocate the vbuffer
    glBindBuffer GL_ARRAY_BUFFER vbuff
    glBufferData GL_ARRAY_BUFFER (fromIntegral vBuffSize) nullPtr GL_STREAM_DRAW

    -- set up the VAO for rendering
    glBindVertexArray vao
    checkGLErrors "Binding the Vertex array"
    glEnableVertexAttribArray posIndex
    checkGLErrors ("Enabling the Vertex pos attrib " ++ (show posIndex))
    glEnableVertexAttribArray uvIndex
    checkGLErrors ("Enabling the Vertex uv attrib " ++ (show uvIndex))

    -- position x,y comes from first 2 Floats
    glVertexAttribPointer posIndex 2 GL_FLOAT 0 16 nullPtr
    -- uv x,y comes from the next 2 Floats
    glVertexAttribPointer uvIndex 2 GL_FLOAT 0 16 (plusPtr nullPtr 8)

    glBindVertexArray 0
    glBindBuffer GL_ARRAY_BUFFER 0

    return $ DrawCtx prog vao vbuff

-- |Set up the necessary state to start drawing a new frame
drawStart :: DrawCtx -> (GLfloat, GLfloat) -> IO ()
drawStart ctx (cameraX, cameraY) = do
    glClear GL_COLOR_BUFFER_BIT
    checkGLErrors "Clearing the renderbuffer"

    -- bind the shader
    glUseProgram (shaderProg ctx)
    checkGLErrors "Binding the main shader"
    diffuseLoc <- getUniformLocation (shaderProg ctx) "diffuseTex"
    checkGLErrors "Getting diffuse uniform location"
    glUniform1i diffuseLoc 0
    checkGLErrors "Setting diffuse texture unit"

    -- set up the camer pos and screen size bits
    cameraloc <- getUniformLocation (shaderProg ctx) "camerapos"
    glUniform2f cameraloc cameraX cameraY
    screensizeloc <- getUniformLocation (shaderProg ctx) "halfscreensize"
    glUniform2f screensizeloc 320.0 240.0
    checkGLErrors "Setting uniforms"

    glBindVertexArray $ vertSpecArray ctx
    checkGLErrors "Binding the VAO"
    glBindBuffer GL_ARRAY_BUFFER $ vertBuffObj ctx
    checkGLErrors "Binding the vbuffer"

-- |Finish off a frame, flushing buffers and so on as required.
drawEnd :: DrawCtx -> IO ()
drawEnd _ = do
    glBindVertexArray 0
    checkGLErrors "Unbinding the VAO"

spriteCoords :: SpriteAndPos -> [GLfloat]
spriteCoords s = [
        -- first tri (0,1,2)
        l, b, tl, tb,
        r, b, tr, tb,
        l, t, tl, tt,
        -- second tri (2,1,3)
        l, t, tl, tt,
        r, b, tr, tb,
        r, t, tr, tt
    ]
    where
        l = fst $ pos s
        r = l + cellX
        b = snd $ pos s
        t = b + cellY
        tex = spriteTex (sprite s)
        tl = (fromIntegral (rem tex atlasWidthCells)) * atlasLeftMul
        tr = tl + atlasRightInc
        tb = 1.0 - (fromIntegral (quot tex atlasWidthCells)) * atlasBottomMul
        tt = tb - atlasTopInc

-- Fills the buffer with the sprites and returns the number of vertices written
fillSpriteBuffer :: [SpriteAndPos] -> Ptr GLfloat -> IO GLsizei
fillSpriteBuffer = fillSpriteBuffer_ 0

-- SECURITY WARNING: Potential buffer overrun location.
-- Doesn't check upper bound. Does pokeArray?
fillSpriteBuffer_ :: GLsizei -> [SpriteAndPos] -> Ptr GLfloat -> IO GLsizei
fillSpriteBuffer_ o [] _ = return o
fillSpriteBuffer_ o (x:xs) b = do
    let coords = spriteCoords x
    let cl = length coords
    pokeArray b coords
--    printVerts (quot cl 4) b
    fillSpriteBuffer_ (o+6) xs $ advancePtr b cl

printVert :: Ptr CFloat -> IO (Ptr CFloat)
printVert b = do
    vals <- peekArray 4 b
    putStrLn $ show vals
    return $ advancePtr b 4

printVerts :: Int -> Ptr CFloat -> IO ()
printVerts 0 _ = do
    putStrLn ""
printVerts i b = do
    newb <- printVert b
    printVerts (i-1) newb

-- |Take the DrawCtx and use it to draw out squares from the Vertex data and
-- texture list. Vertices are assumed to be groups of 4 GLfloats, first 2 for
-- post and second 2 for UV of that vertex. Each square will consume 6 vertices
-- and 2 triangles
drawSprites :: DrawCtx -> [SpriteAndPos] -> GLuint -> IO ()
drawSprites _ sprites atlasTex = do

    glActiveTexture GL_TEXTURE0
    glBindTexture GL_TEXTURE_2D atlasTex

    let withMapBuffer = withMappedBufferRange GL_ARRAY_BUFFER 0 (fromIntegral vBuffSize)
    let accessOpts = [Write]
    vertCount <- withMapBuffer accessOpts fillBuff mapFailed
    glDrawArrays GL_TRIANGLES 0 vertCount

    where
        fillBuff = fillSpriteBuffer sprites
        mapFailed :: MappingFailure -> IO GLsizei
        mapFailed _ = do
            putStrLn "Mapping failed"
            return 0
